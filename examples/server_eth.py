# standard imports
import logging
import json
import os
import base64
import time
import hashlib
from urllib import request

# third-party imports
import gnupg
import confini

# local imports
from ecuth.challenge import AuthChallenge, source_hash
from ecuth.ext.eth import EthereumRetriever
from ecuth.filter.eip712 import EIP712Filter
from ecuth.filter.hoba import HobaFilter
from ecuth.acl import YAMLAcl

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

script_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.dirname(script_dir)

gpg_dir = os.path.join(root_dir, '.gnupg')
gpg = gnupg.GPG(gnupghome=gpg_dir)
logg.debug('gpg dir {}'.format(gpg_dir))

config = confini.Config(os.path.join(root_dir, 'config'))
config.process()


def sha256filter(data):
    """Filter performing sha256 in input

    :param data: Input
    :type data: bytes
    :returns: Digest
    :rtype: bytes
    """
    h = hashlib.sha256()
    h.update(data)
    z = h.digest()
    logg.debug('sha256 filter {} -> {}'.format(data, z))
    return z



def decrypter(data):
    """GNUPG POC decrypter and verifier.
    
    :param data: Ciphertext
    :type data: bytes
    :raises ValueError: Signature cannot be verified
    :return: Plaintext
    :rtype: bytes
    """
    d = gpg.decrypt(data, passphrase='tralala')
    if d.trust_level < d.TRUST_FULLY:
        raise ValueError('untrusted data')
    logg.debug('trust {}'.format(d.trust_level))
    return str(d)


# temporary method, split fetcher from the challenger
def fetcher(address):
    """Uses instance member base_url to retrieve ACL document with http.
    
    :param address: Ethereum address of user
    :type address: str, 0x-hex
    :raises urllib.error.URLError:
    :raises http.client.RemoteDisconnected:
    :return: Retrieved data
    :rtype: bytes
    """
    url = os.path.join(config.get('ECUTH_BASE_URL'), address.hex())
    logg.debug('fetcher retrieving {}'.format(url))
    req = request.Request(url)
    req.add_header('Accept', 'application/octet-stream')
    response = request.urlopen(req)
    r = response.read()
    return decrypter(r)



hoba_filter = HobaFilter(config.get('HTTP_AUTH_ORIGIN'), config.get('HTTP_AUTH_REALM'), 'secp256k1')
eip712_filter = EIP712Filter(config.get('EIP712_NAME'), config.get('EIP712_VERSION'), config.get('ECUTH_CHAIN_ID'))

retriever = EthereumRetriever(fetcher, YAMLAcl)
retriever.add_filter(hoba_filter.filter, 'hoba')
retriever.add_filter(sha256filter)
retriever.add_filter(eip712_filter.filter, 'eip712')


def login_page():
    f = open(os.path.join(script_dir, 'html', 'login.html'))
    d = f.read(1024*1024)
    f.close()
    return d


# UWSGI application
def application(env, start_response):

    for k in env.keys():
        logg.debug('env {} {}'.format(k, env[k]))

    headers = [('Content-Type', 'text/html;charset=UTF-8')]

    auth_state = False

    token = None
    auth = env.get('HTTP_AUTHORIZATION')
    if auth != None:
        (b, token) = auth.split(' ')
        if b.lower() == 'hoba':
           
            hoba_filter.parse(token) 

            identity = retriever.validate(env.get('REMOTE_ADDR'), hoba_filter.challenge, hoba_filter.signature)
            session = retriever.load(identity)
            headers = [
                    ('Content-Type', 'application/json; charset=UTF-8',),
                    ('Cache-Control', 'none',),
                    ('Pragma', 'no-cache',),
                    ]
            #expires_in = int(tokens[2] - time.time())
            expires_in = int(session.auth_expire - time.time())
            data = {
                'access_token': session.auth.hex(),
                'token_type': 'Bearer',
                'expires_in': expires_in,
                'refresh_token': session.refresh.hex(),
                    }
            data_json = json.dumps(data)
            data_bytes = data_json.encode('utf-8')
            start_response('200 OK', headers)
            return [data_bytes]
        

    response = ''
    if auth_state:
        post_data = None
        f = env.get('wsgi.input')
        c = f.read(1)
        if len(c) > 0:
            f.seek(0)
            post_data = json.load(f)
        f.close()
        login_success_message_bytes = "You are logged in".encode('utf-8')
        headers = [
                ('Content-Type', 'text/plain', ),
                ('Content-Length', str(login_success_message_bytes.length),),
                ]

        start_response('200 OK', headers)
        return [login_success_message_bytes]

    else:
        if env.get('REQUEST_URI') == '/request':
            (nonce, expires) = retriever.challenge(env.get('REMOTE_ADDR'))
            nonce_b64 = base64.b64encode(nonce)
            logg.debug('sending nonce {} {}'.format(nonce.hex(), nonce_b64.decode('utf-8')))

            expires_duration = expires - time.time()
            wwwauth_header_value  = 'challenge="{}",max-age="{}",realm="{}"'.format(
                nonce_b64.decode('utf-8'),
                int(expires_duration),
                config.get('HTTP_AUTH_REALM')
            )
            wwwauth_header = ('WWW-Authenticate', 'HOBA {}'.format(wwwauth_header_value))
            headers.append(wwwauth_header)

        response = login_page()

        response_bytes = response.encode('utf-8')
        content_length = len(response_bytes)
        headers.append(('Content-Length', str(content_length),))

        start_response('401 Unauthorized', headers)
        return [response_bytes]
